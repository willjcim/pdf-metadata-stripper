

from django.db import models
from uuid import uuid4

def generateUUID():
        return str(uuid4())

class FileDir(models.Model):
    uuid = models.UUIDField(default=generateUUID, max_length=32, editable=False, unique=True)
    name = models.CharField(max_length=64)
    source = models.CharField(max_length=128)
    destination = models.CharField(max_length=128)
    status = models.CharField(max_length=8)

    def __str__(self):
        return self.name
