from django.contrib import admin
from django.urls import include, path

urlpatterns = [
    path("compress/", include("compress.urls")),
    path("admin/", admin.site.urls),
]
