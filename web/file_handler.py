import os
import shutil

from pdfc.pdf_compressor import compress

def compress_file(name, source, destination):
    # prompt user for file paths 
    while True:
        # take user input
        if os.name == 'nt':
            slash = '\\'
        else:
            slash = '/'
            
        if os.path.exists(source): 
            # change cwd 
            os.chdir(source)
            os.chdir('..' + slash)
            dst = source.split(slash)
            dst.reverse()
            dst = dst[0]
            dst = os.getcwd() + slash + dst
            newFile = dst

            i = 1
            while True:
                if os.path.exists(newFile):
                    newFile = dst + str(i)
                    i = i + 1
                else:
                    # create copy of the directory and its subdirs 
                    dst = shutil.copytree(source, newFile)
                    os.chdir(dst)
                    for dirpath, dirs, files in os.walk("."): # go through all directories and files
                        for filename in files:
                            fname = os.path.join(dirpath, filename)
                            newName = fname[:-4] + "1.pdf"
                            if fname.endswith('.pdf'): # if a file is a pdf, compress it
                                try:
                                    compress(fname, newName, power=4)
                                    if os.path.exists(newName): # if the pdf was compressed, remove the old version and rename the compressed version
                                        os.remove(fname)
                                        os.rename(fname[:-4] + "1.pdf", fname)
                                except:
                                    print("Failed to reduce size of " + filename) 
                    break
            print("Output found at " + newFile)
            shutil.make_archive(name[:-4], "zip", newFile)
            shutil.copyfile(newFile + "/" + name, destination + "/" + name)

            break
        else:
            if source != "":
                print("Invalid input: " + source + " not found")
            break
