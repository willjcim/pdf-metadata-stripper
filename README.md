# PDF Compression of Full Tree

Compresses all pdf in a tree of directories 

Designed for Indiana University Y109 and Y399 under Professor Dina Spechler

Author: William Im

Credit: Theeko74, skjerns

This script takes a directory filled with subdirectories, and creates a copy with any pdf files in the entire tree compressed. This is done by compressing the image quality of the pdf files, without compromising the image quality 

## Installation
Dependency: Python3
- https://www.python.org/downloads/ 

Dependency: Ghostscript
- On MacOSX install via command line 'brew install ghostscript'
- On Windows install binaries via https://www.ghostscript.com/
- Should be default on most linux distros

## Usage
Run the script
```python run.py```

Follow instructions given in PyQt5

## Results
The script will create a second folder containing all of the subdirectories and compressed pdf files. For example, if your input directory was titled "src", the script will send the output to a directory titled "src1"

## Disclaimer
Please note that this script was written for a very specific use case for the administration of one class. It was done hastily and there are probably a lot of bugs that I did not have time to resolve. Feel free to modify it to fit your needs. 

Additionally, I'm terrible with PyQt5 and I'm sorry in advance.